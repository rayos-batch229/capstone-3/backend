const User = require("../models/User.js");
const Product = require("../models/Product.js");
const auth = require("../auth.js")

// product registration

module.exports.addProduct = (reqBody) => {

		let newProduct = new Product({
				productName : reqBody.productName,
				productBrand: reqBody.productBrand,
				description : reqBody.description,
				price : reqBody.price
			});

			
			return newProduct.save().then((product, error) => {
				
				if (error) {
					return false;
				} else {
					return true;
				};

			});
};

// Get All Products

module.exports.getAllProduct = () => {
	return Product.find()
}

// Get Active Products

module.exports.getAllActiveProducts = () => {
	return Product.find({isActive: true}).then(product => {
		return product;
	});
};

// Update Product

module.exports.updateProduct = (reqParams, reqBody) => {

	//if(admin.isAdmin){

		console.log(reqParams);
		let updateProduct = {
			productName : reqBody.productName,
			productBrand: reqBody.productBrand,
			description: reqBody.description,
			price: reqBody.price
		};

		return	Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((product, error) =>{
			
			if(error){
				return false;

		
			} else {
				return true
			}
		})
}

// Retrieve Product

module.exports.retrieveOneProduct = (reqParams) => {

	return Product.findById(reqParams.productId)
}

// Archive Product

module.exports.archiveProducts = (reqParams, data) => {
	
	if (data.isAdmin) {
		let archiveProd = {
			isActive: false
		};
		
		return Product.findByIdAndUpdate(reqParams.productId, archiveProd).then((product, error) =>{

			if(error){
				return false;

			
			} else {
				return true
			}
		})
	}else{
		return ("Authorized Personnel Only");
	}
}

// Retrieve Product

module.exports.retrieveProduct = (reqParams) => {

	let archivedCourse = {
		isActive: true
	};

	return Product.findByIdAndUpdate(reqParams.productId, archivedCourse).then((product, error) => {

		if(error) {
			console.log(error);
			return false;
		} 
		else {
			console.log(product);
			return true;
		}
	});
};



