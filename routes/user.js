const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers.js");
const auth = require("../auth.js");


router.post("/checkEmail", (req, res) => {
	userControllers.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

router.post("/register", (req,res) =>{
	userControllers.registerUser(req.body).then(controllerResult => res.send(controllerResult));
})

router.post("/login", (req,res) =>{
	userControllers.userLogin(req.body).then(controllerResult => res.send(controllerResult));
})

router.post("/details", auth.verify, (req,res) =>{
	const user = {
		userId: auth.decode(req.headers.authorization).id
	}
	userControllers.getUserDetails(user).then(controllerResult => res.send(controllerResult));
})

router.get("/allUsers", auth.verify, (req,res) =>{

	const user = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	userControllers.getAllUser(user).then(controllerResult => res.send(controllerResult));
})

router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {
	const admin = {
		isAdmin:  auth.decode(req.headers.authorization).isAdmin	
	}

	userControllers.addAdminUser(req.params, admin).then(controllerResult => res.send(controllerResult));
})

router.post("/myOrder", auth.verify, (req,res) =>{
	const user = {
		isAdmin:  auth.decode(req.headers.authorization).isAdmin,
		userId: auth.decode(req.headers.authorization).id	
	}
	userControllers.createOrder(req.body,user).then(controllerResult => res.send(controllerResult))
})

router.get("/getAllOrder", auth.verify, (req,res) =>{
	const user = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	userControllers.retrieveOrders(user).then(controllerResult => res.send(controllerResult))
})

router.get("/:userId/allMyOrder", auth.verify, (req,res) =>{
	const admin = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	userControllers.retrieveUserOrders(req.params, admin).then(controllerResult => res.send(controllerResult))
})

module.exports = router;

