const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers.js");
const auth = require("../auth");

// Add prodcuts (Admin)
router.post("/add", (req,res) =>{
	productControllers.addProduct(req.body).then(controllerResult => 
        res.send(controllerResult));
})

// Get all products
router.get("/all", (req, res) => {
    productControllers.getAllProduct().then(controllerResult => {
        res.send(controllerResult)
    });
})

// Get all  Active products
router.get("/active", (req, res) => {

    productControllers.getAllActiveProducts().then(resultFromController => {
        res.send(resultFromController);
    });
});


// Get Single products
router.get("/:productId", (req, res) => {

    productControllers.retrieveOneProduct(req.params).then(controllerResult => {
        res.send(controllerResult)
    });
})

// Update product by id (Admin)
router.put("/update/:productId",  (req,res) => {

        // const admin = {
        //   isAdmin: auth.decode(req.headers.authorization).isAdmin
        // } 

    productControllers.updateProduct(req.params, req.body).then(controllerResult => res.send(controllerResult));
})

//Archive Product (Admin)
router.put("/archive/:productId", auth.verify, (req,res) => {

      const admin = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    productControllers.archiveProducts(req.params, admin).then(resultFromController => res.send(resultFromController));
})

// Retrieve Product
router.put("/retrieve/:productId", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin === true) {
        productControllers.retrieveProduct(req.params).then(resultFromController => {
            res.send(resultFromController);
        });
    }
    else {
        res.send("You are not authorized to commit changes.")
    }
});


module.exports = router;
